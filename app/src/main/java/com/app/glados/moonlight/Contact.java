package com.app.glados.moonlight;

/**
 * Created by Glados on 19/03/2018.
 */

public class Contact {
    private String jid;

    public Contact(String contactJid )
    {
        jid = contactJid;
    }

    public String getJid()
    {
        return jid;
    }

    public void setJid(String jid) {
        this.jid = jid;
    }
}
